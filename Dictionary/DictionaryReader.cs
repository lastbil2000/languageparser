﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Dictionary
{
	public class DictionaryReader<T> where T: IWordsCreator, new()
	{

		private string m_wordsFilename;
		private string m_exceptionsFilename;
		private IWordsCreator m_creator;

		public DictionaryReader (string wordsFilename, string exceptionsFilename, T creator = default(T)) {
			
			m_wordsFilename = wordsFilename;
			m_exceptionsFilename = exceptionsFilename;
			m_creator = creator;

		}

		public void Read(IWordContainer dictionary) {
		
			Console.WriteLine ($"Parsing '{m_wordsFilename}'...");
			int lines = 0;

			Stopwatch sw = new Stopwatch();

			sw.Start();

			IWordsCreator creator = m_creator ?? new T ();

			FileReader f = new FileReader (m_wordsFilename);

			IDictionary<string, IList<string>> exceptions = m_exceptionsFilename?.AsExceptionsFile () ?? new Dictionary<string, IList<string>>();

			foreach (string word in f.GetWords()) {
				
				lines++;

				IEnumerable<IWord> words = creator.Generate (word, exceptions.ContainsKey (word) ? exceptions [word] : new List<string> ());

				foreach (IWord wordx in words) {

					dictionary.AddWord (wordx);

				}

			}


			sw.Stop();

			Console.WriteLine(".. done (Elapsed={0}). Lines:{1}",sw.Elapsed, lines);

		}
	}
}

