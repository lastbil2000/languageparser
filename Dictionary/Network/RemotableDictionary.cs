﻿using System;
using System.Collections.Generic;
using R2Core.Device;

namespace Dictionary
{
	public class RemotableDictionary: DeviceBase, IWordContainer
	{
		private IWordContainer m_dictionary;

		public RemotableDictionary (IWordContainer dictionary): base ("remotable_dictionary") {

			m_dictionary = dictionary;

		}

		public void AddWord (IWord word) {

			m_dictionary.AddWord (word);

		}

		public IEnumerable<IWord> Find (string word) {
			
			IList<IWord> result = new List<IWord> ();
			foreach (IWord foundWord in m_dictionary.Find(word)) {

				IWord parent = null;

				if (foundWord.Parent != null) {
				
					parent = new ChildlessWord (foundWord.Parent.Value, foundWord.Parent.Subtype);

				}

				IWord baseWord = new Word (foundWord.Value, foundWord.Subtype, parent);

				IWord baseWordParent = new ChildlessWord (foundWord.Value, foundWord.Subtype);

				foreach (IWord child in foundWord.Children) {

					baseWord.AddChild (new Word (child.Value, child.Subtype, baseWordParent));

				}

				result.Add (baseWord);
			}

			return result;

		}

	}

	/// <summary>
	/// Use this to avoid reference loops...
	/// </summary>
	class ChildlessWord: Word { public ChildlessWord (string value, GramType type): base(value, type) { }
	
		public override void AddChild (IWord child) {}
	}
}

