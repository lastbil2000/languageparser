﻿using System;
using R2Core.Network;
using R2Core.Data;
using R2Core.Device;
using System.Collections.Generic;

namespace Dictionary
{
	public class RemoteDictionary: DeviceBase, IWordContainer
	{
		private string m_path = "dictionary";
		private string m_remoteDictionaryId = "remotable_dictionary";

		private TCPClient m_client;
		private ISerialization m_serialization;
		private IHostConnection m_connection;
		private dynamic m_remoteDictionary;
		private string m_host;
		private TCPPackageFactory m_factory;
		private int m_port;

		public RemoteDictionary (string host, int port, string path = "dictionary"): base("remote_dictionary")
		{
			
			m_serialization = new JsonSerialization ("serializer");
			m_factory = new TCPPackageFactory (m_serialization);
			m_host = host;
			m_port = port;
			m_path = path;
		}

		public override void Start() {
		
			m_client = new TCPClient ("client", m_factory, m_host, m_port);
			m_client.Timeout = 5000;
			m_client.Start ();
			m_connection = new HostConnection ("connection", m_path, m_client);
			m_remoteDictionary = new RemoteDevice (m_remoteDictionaryId, Guid.NewGuid (), m_connection);

		}

		public override bool Ready {
			get {
				return m_connection.Ready;
			}
		}

		public override void Stop() {
		
			m_client.Stop ();

		}

		public void AddWord (IWord word) {
		
			throw new NotImplementedException ("Can't do this right now...");
			//m_remoteDictionary.AddWord (word);

		}

		public IEnumerable<IWord> Find (string word) {

			dynamic found;
			int retryCount = 0;
			bool gotWords = false;

			while (true) {

				try {

					found = m_remoteDictionary.Find (word);
					break;

				} catch (Exception ex) {

					if (retryCount > 3) {

						throw ex;

					} else {
					
						Start ();
					}
				}
			}
			Console.Write ("x");


			foreach (dynamic wordObject in found) {

				IWord parent = null;

				if(((IDictionary<String, object>)wordObject).ContainsKey("Parent")) {

					if (wordObject.Parent != null) {

						parent = new Word (wordObject.Parent.Value, (GramType)wordObject.Parent.Subtype);

					}

				}

				IWord returnWord = new Word (wordObject.Value, (GramType)wordObject.Subtype, parent);

				foreach (dynamic childObject in wordObject.Children) {

					var willBeAddedSinceTheWordsConstructorDoesThis =
						new Word (childObject.Value, (GramType)childObject.Subtype, returnWord);
				
				}

				yield return returnWord;

			}

		}

	}
}

