﻿using System;
using R2Core.Network;
using R2Core.Data;
using R2Core.Device;

namespace Dictionary
{
	public class DictionaryServer: DeviceBase
	{
		private IServer m_server;
		private ISerialization m_serialization;
		private IWordContainer m_dictionary;
		private IWebEndpoint m_endpoint;

		public DictionaryServer (IWordContainer dictionary, int port, string path = "dictionary"): base("dictionary_server")
		{

			m_dictionary = dictionary;
			m_serialization = new JsonSerialization ("serializer");
			m_server = new TCPServer("server", port, new TCPPackageFactory(m_serialization));
			DeviceRouter receiver = new DeviceRouter ();
			receiver.AddDevice (dictionary);

			m_endpoint = new WebJsonEndpoint (path, receiver, m_serialization);

			m_server.AddEndpoint (m_endpoint);

		}

		public override bool Ready {
			get {
				return m_server.Ready;
			}
		}
		public override void Start() {
		
			m_server.Start ();
		
		}

		public override void Stop() {
		
			m_server.Stop ();

		}
			
	}
}

