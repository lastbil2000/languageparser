﻿using System;
using System.Threading;
using R2Core;
using System.Collections.Generic;

namespace Dictionary
{
	class MainClass
	{
		private static DictionaryServer m_server;

		public static void Main (string[] args)
		{

			Log.Instantiate ("log");
			Log.Instance.AddLogger (new ConsoleLogger ("console"));

			CreateLocationList c = new CreateLocationList ();
			c.Read ("locations.csv");
			c.Write ("index.location");

			return;
//			DictionaryReader<VerbsCreator> verbReader = new DictionaryReader<VerbsCreator> ($"Data/index.verb", $"Data/verb.exc");
//			MammaDictionary localDictionary = new MammaDictionary ("dictionary");
//			verbReader.Read (localDictionary);
//
//			m_server = new DictionaryServer (new RemotableDictionary(localDictionary));
//			m_server.Start ();
//
//			while(!m_server.Ready) { Thread.Sleep (200); Console.Write ("."); } 
//
//			Console.WriteLine ("server up");
//
//			IWordContainer remoteDictionary = new RemoteDictionary ();
//			remoteDictionary.Start ();
//
//			while(!remoteDictionary.Ready) { Thread.Sleep (200); Console.Write ("."); } 
//
//			Console.WriteLine ("client connected");
//
//			string line = null;
//
//			while ((line = Console.ReadLine ()) != String.Empty) {
//			
//				foreach (IWord word in remoteDictionary.Find(line)) {
//
//					if (word.Parent != null) {
//						
//						IWord parent = word.Parent;
//						Console.WriteLine($"PARENT: {parent.Value} : {parent.Type} : {parent.Subtype}");
//					
//					}
//
//					Console.WriteLine($"{word.Value} : {word.Type} : {word.Subtype}");
//					foreach (IWord child in word.Children) {
//
//						Console.WriteLine($"--- {child.Value} : {child.Type} : {child.Subtype}");
//					}
//
//				}
//
//			}
		}

		void Wovov() {
			IList<string> d = new List<string> ();

			d.Add ("a"); d.Add ("b"); d.Add ("c"); d.Add ("d");

			IEnumerator<string> e = d.GetEnumerator ();

			while (e.MoveNext()) {

				string a = e.Current;

				if (a == "b") {

					e.MoveNext ();
				}

				Console.WriteLine (a);

			}

			return;
		}
	}
}
