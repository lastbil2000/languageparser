﻿using System;
using System.Collections.Generic;

namespace Dictionary
{
	public interface IWord {
	
		string Value { get; }

		GramType Type { get; }
		GramType Subtype { get; }

		IWord Parent { get; }

		IEnumerable<IWord> Children { get; }

		void AddChild (IWord child);
	}

	[Serializable]
	public class Word: IWord
	{
		protected string m_value;
		protected IWord m_parent;
		protected GramType m_type;
		protected IList<IWord> m_children;

		public string Value { get { return m_value; } }

		public GramType Subtype { get { return m_type; } }

		public GramType Type { get { return m_parent?.Type ?? m_type; } }

		public IWord Parent { get { return m_parent; } }

		public IEnumerable<IWord> Children { get { return m_children; } }

		public virtual void AddChild (IWord child) {
		
			m_children.Add (child);

		}

		public Word (string value, GramType type, IWord parent = null) {

			m_value = value;
			m_type = type;
			m_parent = parent;
			m_children = new List<IWord> ();
			parent?.AddChild (this);

		}

	}
}

