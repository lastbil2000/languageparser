﻿using System;

namespace Dictionary
{
	public enum GramType
	{
		// These words should never be transcribed
		KeepOriginal = -1,

		// Other
		Root = 0,// root node
		Placeholder = 1, //a-z
		Search = 2, // used when searching in the dictionary

		// Unknown
		Unknown = 3,

		// Adverb
		Adverb = 4,

		// Noun
		Noun = 5,	// man
		Plural = 6,	// men
		Genetive = 7, // man's
		GenetivePlural = 8, // men's

		// Verb
		Verb = 9,		//eat see kill
		Present = 10,	//eats sees kills
		PresentContinous = 11, //eating seeing killing
		Preterite = 12, //ate saw killed
		PastParticiple = 13, //eating seen killed

		// Adjective
		Adjective = 14,
		Comparative = 15,
		Superlative = 16,

		Preposition = 17,

		// Pronomen
		Name = 18,
		Location = 19, 
		Pronomen = 20, // she, I
		Dative = 21, // her, my
		Relative = 22, // what, who

		// Determiner
		Article = 23,
		DT_a = 24,	// a
		DT_an = 25,  // an
		DT_the = 26, // the
		Determiner = 27,  // each, that, some etc..

		//Conjugation
		Conjugation = 28, // and, but etc

		// Other
		Other = 29, // i.e "there"
		Annotation = 30, // . , etc
		Symbol = 31 // '

	}

}

