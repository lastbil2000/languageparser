﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.IO;

namespace Dictionary
{
	public class FileReader
	{
		private string m_fileName;

		private Regex m_wordRegexp = new Regex (@"^[a-z\-\'_\.\!\?\,\’\\]+");

		public FileReader (string fileName) {

			m_fileName = fileName;

			if (!File.Exists (fileName)) {
			
				throw new IOException ($"Filename: {fileName} does not exist.");

			}
		}


		public IEnumerable<string> GetWords() {
		
			string line = null;

			using (StreamReader reader = new StreamReader (m_fileName)) {

				while ((line = reader.ReadLine ())?.Trim() != null) {

					if (line.StartsWith("#")) { continue; } // Comment

					if (line.Trim ().Length > 0) {
					
						yield return line.Replace ("_", " ");

					}

//					Match wordMatch = m_wordRegexp.Match (line.ToLower());
//
//					if (wordMatch.Success) {
//
//						yield return wordMatch.Value.Replace("_", " ");
//
//					}

				}

			}

		}

		public IDictionary<string,string> GetWordsDictionary() { 
		
			IDictionary<string,string> replacements = new Dictionary<string, string> ();

			foreach(KeyValuePair<string,string> replacement in _GetWordsDictionary ()) {

				replacements.Add (replacement);

			}

			return replacements;

		}
		/// <summary>
		/// File should be in format [word] [word] (where the later has '_'-separated spaces which will be replaced).
		/// </summary>
		/// <returns>The words dictionary.</returns>
		private IEnumerable<KeyValuePair<string,string>> _GetWordsDictionary() {

			string line = null;

			using (StreamReader reader = new StreamReader (m_fileName)) {

				while ((line = reader.ReadLine ())?.Trim() != null) {

					if (line.StartsWith("#")) { continue; } // Comment

					string[] words = line.Split (null);

					if (words.Length == 0) { continue; } 
					if (words.Length != 2) { 
					
						Console.WriteLine ($"Invalid line: '{line}'. Contains {words.Length} parts.");
						continue; 

					}

					yield return new KeyValuePair<string, string>(words[0], words[1].Replace ("_", " "));

//					Match wordMatch = m_wordRegexp.Match (words[0].ToLower());
//
//					if (wordMatch.Success) {
//
//						string key = wordMatch.Value.Replace ("_", " ");
//						yield return new KeyValuePair<string, string>(key, words[1].Replace ("_", " ")) ;
//
//					}

				}

			}

		}

	}
}

