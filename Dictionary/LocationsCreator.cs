﻿using System;
using System.Collections.Generic;

namespace Dictionary
{
	public class LocationsCreator: IWordsCreator
	{

		public IEnumerable<IWord> Generate(string word, IEnumerable<string> ex) {

			IList<IWord> words = new List<IWord> ();

			yield return new Word (word, GramType.Location);

		}

	}

}

