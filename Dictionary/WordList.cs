﻿using System;
using System.Collections.Generic;
using Dictionary;

namespace Dictionary
{
	public class WordList
	{

		private List<string> m_words;
		public IList<string> Words { get { return m_words; } } 

		public WordList ()
		{

			m_words = new List<string> ();

		}

		public void Load(string filename) {
		
			m_words.AddRange (new FileReader(filename).GetWords());

		}

	}
}

