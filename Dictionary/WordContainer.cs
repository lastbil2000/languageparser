﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Dictionary
{

	[Serializable]
	public class WordContainer { 

		public IWord Word;
		public List<WordContainer> Children;

		public WordContainer(IWord word) {

			Word = word;
			Children = new List<WordContainer> ();

		}

		public void Add(WordContainer container) {


			if (Word.Type == GramType.Root && container.Word.Type != GramType.Placeholder) {
				//This is the first node

				int pos = (int)Encoding.ASCII.GetBytes(container.Word.Value.ToLower())[0] - 97;

				// Add it to the placeholder location for letters
				if (pos >= 0 && pos < 124 - 97) {
				
					Children [pos].Add (container);
					return;
				}

				pos = (int)Encoding.ASCII.GetBytes(container.Word.Value.ToLower())[0] - 48;

				if (pos >= 0 && pos < 59 - 48) {

					//add it to location for numbers
					Children [pos + 26].Add (container);
					return;
				
				} else {
				
					// everything else
					Children [26 + 10].Add (container);

				}

			}

			for (int i = 0; i < Children.Count; i++) {

				if (Children [i].IsParentOf (container)) {
					
					Children [i].Add (container);

					return;

				}

				if (Children [i].IsChildOf (container)) {

					container.Add (Children [i]);
					Children.RemoveAt (i);
					i--;

				}

			}

			Children.Add (container);

		}

		public bool IsParentOf(WordContainer newWord) {

			if (Word.Type != GramType.Placeholder && newWord.Word.Type != Word.Type) { return false; }

			if (newWord.Word.Value.Length < Word.Value.Length ) {

				return false;

			}

			return Word.Value.ToLower() == newWord.Word.Value.ToLower().Substring(0,Word.Value.Length);

		}

		public bool IsChildOf(WordContainer newWord) {

			if (Word.Type != GramType.Search &&  
				newWord.Word.Type != GramType.Placeholder && 
				newWord.Word.Type != Word.Type) { return false; }

//			if (Word.Type == "place_holder" && newWord.Word.Value.StartsWith (Word.Value)) {
//				
//				return true;
//			
//			}

			if (newWord.Word.Value.Length > Word.Value.Length - 1) {

				return false;

			}

			return newWord.Word.Value.ToLower() == Word.Value.ToLower().Substring(0,newWord.Word.Value.Length);

		}

		public IList<WordContainer> Matches(WordContainer word) {

			List<WordContainer> matches = new List<WordContainer> ();

			foreach (WordContainer child in Children) {

				if (child.Word.Type != GramType.Placeholder && word.Word.Value == child.Word.Value) {

					matches.Add (child);

				}

				if (word.IsChildOf (child)) {

					matches.AddRange (child.Matches (word));

				} 

			}

			return matches;

		}


		public void Print(int trav) {

			for (int i = 0; i < trav; i++) {

				Console.Write("-");

			}

			Console.WriteLine ($"[{Word.Type}] {Word.Value}");

			foreach (WordContainer container in Children) {

				container.Print (trav + 1);

			}
		}

	}

}

