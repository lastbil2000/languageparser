﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Dictionary
{
	public class AdjectiveCreator: IWordsCreator {

		private Regex m_syllables = new Regex ("[AaOoEeUuIi-]+");
		//A man with no name tries to reunite with his family in Purgatory.

		public IEnumerable<IWord> Generate(string word, IEnumerable<string> exceptions) {

			IList<IWord> words = new List<IWord> ();

			IWord baseWord = new Word (word, GramType.Adjective);

			words.Add (baseWord);

			if (exceptions.Count() == 2) {

				//Ignore all other. presume that the first word is the comparative conjugation

				words.Add (new Word (exceptions.First(), GramType.Comparative, baseWord));
				words.Add (new Word (exceptions.Last(), GramType.Superlative, baseWord));

			} else {

				words.Add (new Word (GetComparative (word), GramType.Comparative, baseWord));
				words.Add (new Word (GetSuperlative (word), GramType.Superlative, baseWord));

			}

			return words;

		}

		private string GetComparative(string word) {

			if (word.EndsWith ("y")) { return word.Substring (0, word.Length - 1) + "ier"; } 

			if (m_syllables.Matches (word).Count > 2) { return "more " + word; }

			if (word.EndsWith ("e")) { return word + "r"; }

			return word + "er";

		}

		private string GetSuperlative(string word) {

			if (word.EndsWith ("y")) { return word.Substring (0, word.Length - 1) + "iest"; } 

			if (m_syllables.Matches (word).Count > 2) { return "most " + word; }

			if (word.EndsWith ("e")) { return word + "st"; }

			return word + "est";

		}

	}

}
