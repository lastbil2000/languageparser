﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Dictionary
{
	public class NounsCreator: IWordsCreator {

		// A list of uncountable (i.e. wood, money) nouns.
		private IEnumerable<string> m_uncountables;

		public NounsCreator() {}

		public NounsCreator(IEnumerable<string> uncountables) {
		
			m_uncountables = uncountables;

		}

		//A man with no name tries to reunite with his family in Purgatory.

		public IEnumerable<IWord> Generate(string word, IEnumerable<string> exceptions) {

			IList<IWord> words = new List<IWord> ();

			if (word.Length < 2) { return words; }

			IWord baseWord = new Word (word, GramType.Noun);

			if (m_uncountables?.Contains (word) == true) {

				// uncountables should always be plural
				words.Add(new Word (word, GramType.Plural, baseWord));
				return words;

			}

			words.Add (baseWord);
			words.Add (new Word (GetGenetive(word), GramType.Genetive, baseWord));

			string exception = exceptions.FirstOrDefault ();

			if (exception != null) {

				words.Add (new Word (exception, GramType.Plural, baseWord));

			} else {
				
				words.Add (new Word (GetPlural (word), GramType.Plural, baseWord));

			}

			words.Add (new Word (GetGenetive(words.Last().Value), GramType.GenetivePlural, baseWord));

			return words;

		}

		private string GetGenetive(string word) {
		
			return word + "'s";

		}

		private string GetPlural(string word) {
			
			if (word.EndsWith ("ey")) {

				return word.Substring (0, word.Length - 2) + "ies";

			} else if (word.EndsWith ("y") && word.Substring(word.Length - 2,1).HasNoVowels()) {

				return word.Substring (0, word.Length - 1) + "ies";

			} else if (word.EndsWith ("s")) {

				return word + "es";

			}

			return word + "s";

		}

	}


}

