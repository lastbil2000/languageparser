﻿using System;
using System.Collections.Generic;

namespace Dictionary
{
	public class PrepositionsCreator: IWordsCreator
	{
		//A man with no name tries to reunite with his family in Purgatory.

		public IEnumerable<IWord> Generate(string word, IEnumerable<string> exceptions) {

			IList<IWord> words = new List<IWord> ();

			IWord baseWord = new Word (word, GramType.Adverb);

			words.Add (baseWord);

			return words;

		}

	}
}

