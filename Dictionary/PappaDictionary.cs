﻿using System;
using R2Core.Device;
using System.Collections.Generic;

namespace Dictionary
{
	public class PappaDictionary: DeviceBase, IWordContainer
	{
		private IDictionary<string, IList<IWord>> m_dictionary;

		public PappaDictionary (string id) : base(id)
		{
			m_dictionary = new Dictionary<string, IList<IWord>> ();
		}

		public void AddWord (IWord word) {
		
			if (!m_dictionary.ContainsKey (word.Value)) {
			
				m_dictionary [word.Value] = new List<IWord> ();

			}

			m_dictionary [word.Value].Add (word);

		}

		public IEnumerable<IWord> Find (string word) {
		
			if (m_dictionary.ContainsKey (word)) {
			

				foreach (IWord wordObject in m_dictionary [word]) {
				
					yield return wordObject;

				}

			}

			yield break;

		}

	}

}

