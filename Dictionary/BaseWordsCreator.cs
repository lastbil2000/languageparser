﻿using System;
using System.Collections.Generic;

namespace Dictionary
{
	public abstract class BaseWordsCreator
	{

		private IEnumerable<string> m_exceptions;

		protected IEnumerable<string> Exceptions { get { return m_exceptions; } }

		public BaseWordsCreator (IEnumerable<string> exceptions) {
			
			m_exceptions = exceptions ?? new List<string>();

		}

		public abstract IEnumerable<IWord> Generate (string word);

	}
}

