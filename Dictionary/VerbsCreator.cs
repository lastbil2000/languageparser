﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dictionary
{

	public class VerbsCreator: IWordsCreator
	{

		public IEnumerable<IWord> Generate(string word, IEnumerable<string> ex) {

			IList<IWord> words = new List<IWord> ();

			IWord baseWord = new Word (word, GramType.Verb);

			words.Add (baseWord);
			words.Add (new Word (GetPresent (word), GramType.Present, baseWord));
			words.Add (new Word (GetPresentContinous (word), GramType.PresentContinous, baseWord));

			IList<string> exceptions = new List<string> (ex);

			if (exceptions.Count > 0) {

				words.Add (new Word (exceptions [0], GramType.Preterite, baseWord));
				words.Add (new Word (exceptions [1], GramType.PastParticiple, baseWord));

			} else {

				words.Add (new Word (GetPast (word), GramType.Preterite, baseWord));
				words.Add (new Word (GetPast (word), GramType.PastParticiple, baseWord));

			}

			return words;

		}

		private string GetPresent(string word) {

			// words ending with oy,ay etc is ignored from the y->ie rule
			if (!(word.Length > 1 && word.Rev(1).StartsWithAWovel()) && word.EndsWith ("y")) {

				return word.Substring (0, word.Length - 1) + "ies";

			} else if (word.EndsWith ("s") || word.EndsWith ("o") || word.EndsWith ("h")) {

				return word + "es";

			}

			return word + "s";

		}

		//killing
		private string GetPresentContinous(string word) {

			if (new string[] {"b","g"}.Contains(word.LastCharacter ()) && !word.LastCharacter (2).HasNoVowels ()) {

				return word + word.LastCharacter () + "ing";

			}

			if (word.EndsWith ("e") && word[word.Length - 2] != 'e') {

				return word.Substring (0, word.Length - 1) + "ing";

			}

			return word + "ing";

		}

		//killed
		private string GetPast(string word) {

			if (new string[] {"b","g", "m", "d", "j", "p", "r"}.Contains(word.LastCharacter ()) && !word.LastCharacter (2).HasNoVowels ()) {
			
				return word + word.LastCharacter () + "ed";

			}

			if (word.EndsWith ("e")) {

				return word + "d";

			}

			return word + "ed";

		}

	}

}

