﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Dictionary
{
	public static class CrazyStringExtensions
	{

		public static string UppercaseFirstLetter(this string self) {
		
			return self.Substring (0, 1).ToUpper () + self.Substring (1, self.Length - 1);

		}

		public static string UppercaseEachFirstLetter(this string self) {
		
			//TODO
			return self.UppercaseFirstLetter();

		}

		public static bool IsAlphabetic(this string self) {
		
			Regex alphaNumerical = new Regex (@"^[a-zA-Z]+") ;

			return alphaNumerical.IsMatch (self);

		}

		public static bool StartsWithAWovel(this string self) {

			Regex vowels = new Regex(@"^[aeiou]+");

			return vowels.IsMatch(self.ToLower());

		}

		public static bool HasNoVowels(this string self) {

			string vowels = "aeiou";

			return self.Count (vowels.Contains) == 0;
		}

		public static string LastCharacter(this string self, int pos = 1) {
		
			return self.Substring (self.Length - pos, 1);
		
		}

		/// <summary>
		/// Returns the character at index Length - pos
		/// </summary>
		/// <param name="self">Self.</param>
		/// <param name="pos">Position.</param>
		public static string Rev(this string self, int pos) {

			return self.Substring (self.Length - pos - 1, 1);

		}

		/// <summary>
		/// Considers self to be a file name. Reads the file and return the exception dicitonary
		/// </summary>
		/// <returns>The exceptions file.</returns>
		/// <param name="self">Self.</param>
		public static IDictionary<string, IList<string>> AsExceptionsFile(this string self) {

			var exceptions = new Dictionary<string, IList<string>> ();

			string line = null;
			StreamReader reader;

			using (reader = new StreamReader (self)) {

				while ((line = reader.ReadLine ()) != null) {

					string[] parts = line.Split (null);

					string word = parts.First().Replace("_", " ");
					IList<string> subwords = new List<string> ();

					for (int i = 1; i < parts.Length; i++) {
					
						subwords.Add(parts [i].Replace("_", " "));

					}

					if (exceptions.ContainsKey (word)) {

						foreach (string subword in subwords) {
						
							exceptions [word].Add (subword);

						}


					} else {

						exceptions.Add (word, new List<string> ());

						foreach (string subword in subwords) {

							exceptions [word].Add (subword);

						}

					}

				}

			}

			return exceptions;

		}
	}
}

