﻿using System;
using System.Collections.Generic;
using R2Core.Device;

namespace Dictionary
{
	public interface IWordContainer : IDevice {
	
		void AddWord (IWord word);
		IEnumerable<IWord> Find (string word);

	}

	public class MammaDictionary: DeviceBase, IWordContainer
	{

		private Dictionary<string, WordContainer> m_trees;
		private WordContainer m_tree;
		private IList<IWord> m_words;
		private bool m_storeList;

		/// <summary>
		/// If storelist is true, there will be an additonal list. quite stupid.
		/// </summary>
		/// <param name="id">Identifier.</param>
		/// <param name="storeList">If set to <c>true</c> store list.</param>
		public MammaDictionary (string id, bool storeList = false) : base(id)
		{
			throw new NotImplementedException ("Use PappaDictionary instead.");
			m_words = new List<IWord> ();
			m_tree = new WordContainer (new Word ("", GramType.Root));
			m_trees = new Dictionary<string, WordContainer> ();
			m_storeList = storeList;

			for (int i = 97; i < 123; i++) {
			
				//Add placeholders for a-z
				m_tree.Add(new WordContainer(new Word(((char)i).ToString(), GramType.Placeholder)));

			}

			for (int i = 0; i < 10; i++) {

				//Add placeholders for 0-9
				m_tree.Add(new WordContainer(new Word(i.ToString(), GramType.Placeholder)));

			}

			//Add placeholders for others i.e. "."
			m_tree.Add(new WordContainer(new Word(".", GramType.Placeholder)));


		}

		public IEnumerable<IWord> AllWords { get { return m_words; } }

		#region IDictionary implementation

		public void AddWord (IWord word)
		{
			string wordToAdd = word.Value;

			if (string.IsNullOrEmpty (word.Value?.Trim ())) {
			
				Console.WriteLine ("Not adding empty/whitespace/null word");
				return;

			}

			if (m_storeList) {
			
				m_words.Add (word);

			}

			m_tree.Add (new WordContainer(word));

		}

		public IEnumerable<IWord> Find (string word) {

			foreach (WordContainer container in m_tree.Matches(new WordContainer(new Word (word, GramType.Search)))) {
				
				yield return container.Word;

			}
		
		}

		public void Print() {

			//Console.WriteLine (m_tree.Children[0].Children.Count);
			m_tree.Print (0);

		}

		#endregion
	}
}

