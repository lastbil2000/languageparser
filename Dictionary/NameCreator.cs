﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dictionary
{
	public class NameCreator: IWordsCreator
	{

		public IEnumerable<IWord> Generate(string word, IEnumerable<string> exceptions) {

			IList<IWord> words = new List<IWord> ();

			if (word.Length < 2) { return words; }

			IWord baseWord = new Word (word, GramType.Name);

			words.Add (baseWord);

			words.Add (new Word (GetGenetive(word, true), GramType.Genetive, baseWord));

			string exception = exceptions.FirstOrDefault ();

			if (exception != null) {

				words.Add (new Word (exception, GramType.Plural, baseWord));

			} else {

				words.Add (new Word (GetPlural (word), GramType.Plural, baseWord));

			}

			words.Add (new Word (GetGenetive(words.Last().Value, true), GramType.GenetivePlural, baseWord));

			return words;

		}

		private string GetGenetive(string word, bool useApostrophe) {

			return word + (useApostrophe ? "'" : "") + "s";

		}

		private string GetPlural(string word) {

			if (word.EndsWith ("ey")) {

				return word.Substring (0, word.Length - 2) + "ies";

			} else if (word.EndsWith ("y")) {

				return word.Substring (0, word.Length - 1) + "ies";

			} else if (word.EndsWith ("s")) {

				return word + "es";

			}

			return word + "s";

		}
	}
}

