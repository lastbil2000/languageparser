﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Dictionary
{
	/// <summary>
	/// Use this to remove words from a file contained in another
	/// </summary>
	public class CrossRemoveWords
	{
		private IEnumerable<string> m_excluded;
		private string m_baseDir;

		public CrossRemoveWords (string baseDir, string excludeFileName) {

			m_baseDir = baseDir;
			excludeFileName = $"{m_baseDir}{Path.DirectorySeparatorChar}{excludeFileName}";
			m_excluded = new FileReader(excludeFileName).GetWords();


		}

		public void Exclude(string inputFileName, string outputFileName) {

			inputFileName = $"{m_baseDir}{Path.DirectorySeparatorChar}{inputFileName}";
			FileReader r = new FileReader (inputFileName);
			int i = 0;

			using (StreamWriter writer = new StreamWriter (outputFileName)) {
				
				foreach (string word in r.GetWords()) {

					i++;

					if (i%1000 == 0) { Console.Write ("."); }

					if (m_excluded.Contains(word)) {

						Console.WriteLine($"Removing: {word}");

					} else { writer.WriteLine (word); }  
				}

			}
		
		}
	}
}

