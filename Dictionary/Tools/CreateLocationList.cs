﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Dictionary
{

	public class LocationWord: IWord {

		private string m_name;
		public string Value { get {return m_name; } }

		public GramType Type { get { return GramType.Location; } }
		public GramType Subtype { get { return GramType.Location; } }

		public IWord Parent { get { return null; } }

		public IEnumerable<IWord> Children { get { return new List<IWord> (); } }

		public void AddChild (IWord child) {}

		public LocationWord(string name) {
			m_name = name;
		}
	
	}
	/// <summary>
	/// Creates a wordlist from a CSV-file containing locations
	/// </summary>
	public class CreateLocationList
	{
		private MammaDictionary m_dictionary;

		private Regex m_numerical = new Regex(@"^[0-9]+$");
		public CreateLocationList ()
		{
			
			m_dictionary = new MammaDictionary("it", true);

		}

		public void Read(string locationsCsvFile) {
		
			Console.WriteLine ("Reading locations");

			int c = 0;
			using (StreamReader reader = new StreamReader (locationsCsvFile)) {

				string line = "";

				while ((line = reader.ReadLine ())?.Trim () != null) {
				
					foreach (string p in line.Split(',')) {

						string part = p.Trim ();

						if (!string.IsNullOrEmpty(part) && !m_numerical.IsMatch(part) && m_dictionary.Find(part).Count() == 0) {

							c++;
							m_dictionary.AddWord (new LocationWord (part));
							//Console.Write (".");

							if (c % 1000 == 0) { Console.Write ($"[{c}]"); }

						}

					}

				}

			}

		}

		public void Write(string outputFile) {
		
			Console.WriteLine ("Writing locations");

			using (StreamWriter writer = new StreamWriter (outputFile)) {

				foreach (IWord word in m_dictionary.AllWords) {

					writer.WriteLine (word.Value);

				}
			}

		}


	}
}

