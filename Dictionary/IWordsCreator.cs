﻿using System;
using System.Collections.Generic;

namespace Dictionary
{
	public interface IWordsCreator
	{

		System.Collections.Generic.IEnumerable<IWord> Generate(string word, IEnumerable<string> exceptions);

	}
}

