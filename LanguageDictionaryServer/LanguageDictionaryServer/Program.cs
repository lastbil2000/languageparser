﻿using System;
using Dictionary;
using System.Threading;
using R2Core;
using System.IO;

namespace LanguageDictionaryServer
{
	class MainClass
	{
		private static DictionaryServer m_server;
		private static string m_dataDir = "Data" + Path.DirectorySeparatorChar;

		public static void Main (string[] args)
		{

			if (args.Length == 1) {
				
				m_dataDir = $"{args[0]}{Path.DirectorySeparatorChar}{m_dataDir}";

			}

			Log.Instantiate ("log");
			Log.Instance.AddLogger (new SimpleConsoleLogger ("loggish", 50));

			PappaDictionary localDictionary = new PappaDictionary ("dictionary");

			DictionaryReader<NounsCreator> prepositionsReader = new DictionaryReader<NounsCreator> ($"{m_dataDir}index.prepositions", null);
			DictionaryReader<VerbsCreator> verbReader = new DictionaryReader<VerbsCreator> ($"{m_dataDir}index.verb.full", $"{m_dataDir}verb.exc");
			//DictionaryReader<AdverbCreatorr> adverbReader = new DictionaryReader<AdverbCreatorr> ($"{m_baseDir}index.adv", null);

			DictionaryReader<AdjectiveCreator> adjectiveReader = new DictionaryReader<AdjectiveCreator> ($"{m_dataDir}index.adj", $"{m_dataDir}adj.exc");


			// The nouns creator has to be created manualy in order to contain the uncountable nouns list
			WordList unclountableNouns = new WordList ();
			unclountableNouns.Load ($"{m_dataDir}noun.uncountable");
			NounsCreator nounsCreator = new NounsCreator (unclountableNouns.Words);

			DictionaryReader<NounsCreator> nounReader = new DictionaryReader<NounsCreator> ($"{m_dataDir}index.noun", $"{m_dataDir}noun.exc", nounsCreator);

			DictionaryReader<LocationsCreator> locationsReader = new DictionaryReader<LocationsCreator> ($"{m_dataDir}index.location", null);

			prepositionsReader.Read (localDictionary);
			verbReader.Read (localDictionary);
			nounReader.Read (localDictionary);
			locationsReader.Read (localDictionary);

			Serializer s = new Serializer ();
			s.SerializeObject<PappaDictionary> (localDictionary, "dictionary.serialized");

			m_server = new DictionaryServer (new RemotableDictionary(localDictionary), 4242);
			m_server.Start ();

			while(!m_server.Ready) { Thread.Sleep (200); Console.Write ("."); } 

			Console.WriteLine ("server up. type 'exit' to quit.");

			IWordContainer remoteDictionary = new RemoteDictionary ("127.0.0.1", 4242);
			remoteDictionary.Start ();

			while(!remoteDictionary.Ready) { Thread.Sleep (200); Console.Write ("."); } 

			Console.WriteLine ("client connected. type 'exit' to quit.");

			string line = null;

			while ((line = Console.ReadLine ()) != "exit") {

				try {
					
				foreach (IWord word in remoteDictionary.Find(line)) {

					if (word.Parent != null) {

						IWord parent = word.Parent;
						Console.WriteLine($"PARENT: {parent.Value} : {parent.Type} : {parent.Subtype}");

					}

					Console.WriteLine($"{word.Value} : {word.Type} : {word.Subtype}");
					foreach (IWord child in word.Children) {

						Console.WriteLine($"--- {child.Value} : {child.Type} : {child.Subtype}");
					}

				}
				} catch (Exception ex) {
				
					Console.WriteLine (ex.Message);

					if (!remoteDictionary.Ready) {
						
						remoteDictionary.Start ();
					
					}
				
				}

			}

		}
	}
}
