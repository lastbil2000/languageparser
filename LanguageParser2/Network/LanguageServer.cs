﻿using System;
using R2Core;
using R2Core.Network;
using R2Core.Data;
using R2Core.Device;

namespace LanguageParser2
{
	public class LanguageServer: DeviceBase
	{

		private IServer m_server;
		private ISerialization m_serialization;
		private IWebEndpoint m_endpoint;
		private IWebEndpoint m_files;

		public LanguageServer (RemoteThing thing, int port, string path = "story") : base ("LanguageServer")
		{

			m_serialization = new JsonSerialization ("serializer");
			m_server = new HttpServer ("http_server", port, m_serialization);
			DeviceRouter receiver = new DeviceRouter ();
			receiver.AddDevice (thing);

			m_endpoint = new WebJsonEndpoint (path, receiver, m_serialization);
			m_files = new WebFileEndpoint ("./", "xyz");

			m_server.AddEndpoint (m_endpoint);
			m_server.AddEndpoint (m_files);

		}

		public void Start() {
		
			m_server.Start ();
		
		}

		public void Stop() {
		
			m_server.Stop ();

		}


	}
}

