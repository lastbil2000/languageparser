﻿using System;
using R2Core.Network;
using R2Core.Data;
using System.Web;

namespace LanguageParser2
{

	/// <summary>
	/// The port numbers for the OpenEr server instances
	/// </summary>
	public enum OpenErType: int {
	
		Identify = 1239, //"language-identifier-server"
		Tokenize = 1240, //"tokenizer-server"
		PosTag = 1241	//"post-tagger-server"

	}

	/// <summary>
	/// Use to communicate with OpenEr instances
	/// </summary>
	public class OpenErClient {
	
		private string m_baseUrl;
		private IMessageClient m_client;

		public OpenErClient (string baseUrl = "http://localhost")
		{
			m_client = new HttpClient ("client", new JsonSerialization (""));
			m_baseUrl = baseUrl;
		}

		public string Send(string data, OpenErType type) {

			HttpMessage msg = new HttpMessage ();

			msg.Method = "POST";

			msg.Destination = $"{m_baseUrl}:{(int)type}";

			msg.Payload = $"kaf=true&input={HttpUtility.UrlEncode (data)}";

			INetworkMessage response = m_client.Send (msg);

			return response.Payload;

		}

	}

}

