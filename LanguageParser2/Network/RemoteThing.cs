﻿using System;
using R2Core.Device;

namespace LanguageParser2
{

	public struct RemoteThingResponse {
	
		public string Story;
		public string Json;

	}

	public class RemoteThing: DeviceBase
	{
		private StoryCreator m_storyCreator;
		private Tokenizer m_tokenizer;

		public RemoteThing (Tokenizer tokenizer, StoryCreator storyCreator) : base("thing") {

			m_storyCreator = storyCreator;
			m_tokenizer = tokenizer;

		}

		public RemoteThingResponse CreateStory(string inputText) {
		
			RemoteThingResponse response = new RemoteThingResponse ();

			response.Json = m_tokenizer.Tokenize (inputText);
			response.Story = m_storyCreator.CreateStory (response.Json);

			return response;

		}

		public void Reload() {

			m_storyCreator.Load ();

		}

	}

}

