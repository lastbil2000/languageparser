﻿using System;
using Dictionary;

namespace LanguageParser2
{
	public struct PreparedWord {

		public int Id;
		//public string Value;
		public string Lemma; 
		public GramType Type;
		public GramType Subtype;
		//public string TypeDescription;
		//public string SubtypeDescription;
		public bool Use;

		public PreparedWord(int id, IWord word) {

			Id = id;
			//Value = word.Value;
			Lemma = word.Parent?.Value ?? word.Value;
			Type = word.Type;
			Subtype = word.Subtype;
			//TypeDescription = word.Type.ToString ();
			//SubtypeDescription = word.Subtype.ToString ();

			// If the word is of type Word or if it's a name, location or noun, allows story creators to use it
			Use = word is Word || (word.Type == GramType.Name || word.Type == GramType.Location || word.Type == GramType.Noun);


		}
		//public GramTypes
	}

}

