#!/bin/bash

jruby -S language-identifier-server -p 1239 & jruby -S tokenizer-server -p 1240 & jruby -S pos-tagger-server -p 1241 & echo "Servers will soon be online..."

#ps ax | grep language-identifier-server | grep -Eo '^[0-9]+' 
