ONCE THERE WERE two brothers who inherited their father's land .  The two brothers divided the land in half and each one farmed his own section .  Over time ,  the older brother married and had six children ,  while the younger brother never married . 

One night ,  the younger brother lay awake .   " It's not fair that each of us has half the land to farm ,  "  he thought .   " My brother has six children to feed and I have none .  He should have more grain than I do .  " 


So that night the younger brother went to his silo ,  gathered a large bundle of wheat ,  and climbed the hill that separated the two farms and over to his brother's farm .  Leaving the wheat in his brother's silo ,  the younger brother returned home ,  feeling pleased with himself . 

Earlier that very same night ,  the older brother was also lying awake .   " It's not fair that each of us has half the land to farm ,  "  he thought .   " In my old age my wife and I will have our grown children to take care of us ,  not to mention grandchildren ,  while my brother will probably have none .  He should at least sell more grain from the fields now so he can provide for himself with dignity in his old age .  " 

So that night ,  too ,  he secretly gathered a large bundle of wheat ,  climbed the hill ,  left it in his brother's silo ,  and returned home ,  feeling pleased with himself . 


The next morning ,  the younger brother was surprised to see the amount of grain in his barn unchanged .   " I must not have taken as much wheat as I thought ,  "  he said ,  bemused .   " Tonight I will be sure to take more .  " 

That very same moment ,  his older brother was also standing in his barn ,  musing much the same thoughts . 

After night fell ,  each brother gathered a greater amount of wheat from his barn and in the dark ,  secretly delivered it to his brother's barn .  The next morning ,  the brothers were again puzzled and perplexed .   " How can I be mistaken ?  "  each one scratched his head .   " There's the same amount of grain here as there was before I cleared the pile for my brother .  This is impossible !  Tonight I will make no mistake - I will take the pile down to the very floor .  That way I will be sure the grain gets delivered to my brother .  " 

The third night ,  more determined than ever ,  each brother gathered a large pile of wheat from his barn ,  loaded it onto a cart ,  and slowly pulled his haul through the fields and up the hill to his brother's barn .  At the top of the hill ,  under the shadow of a moon ,  each brother noticed a figure in the distance .   Who could it be ? 

When the two brothers recognized the form of the other brother and the load he was pulling behind ,  they realized what had happened .  Without a word ,  they dropped the ropes to their carts and embraced . 
