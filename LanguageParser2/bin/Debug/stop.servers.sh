#!/bin/bash

LID=$(pgrep -f language-identifier-server)
TID=$(pgrep -f tokenizer-server)
PID=$(pgrep -f pos-tagger-server)

kill $LID
kill $TID
kill $PID
echo "Servers has been terminated"