#!/bin/bash
#cat $1 | language-identifier | tokenizer | pos-tagger > $2

#curl -d "input=$(cat $1)&kaf=true" http://localhost:1239 -XPOST > $1.identified.xml
echo "--- identifying ---"
echo "input=$(cat $1)&kaf=true" > $1.raw.send
curl -d @$1.raw.send http://localhost:1239 -XPOST > $1.identified.xml

echo "--- tokenizing ---"
#curl -d "input=$(cat $1.identified.xml)&kaf=true" http://localhost:1240 -XPOST > $1.tokenized.tmp
echo "input=$(cat $1.identified.xml)&kaf=true" > $1.identified.send
curl -d @$1.identified.send http://localhost:1240 -XPOST > $1.tokenized.xml

echo "--- pos-tagging ---"
echo "input=$(cat $1.tokenized.xml)&kaf=true" > $1.tokenized.send
curl -d @$1.tokenized.send http://localhost:1241 -XPOST > $2