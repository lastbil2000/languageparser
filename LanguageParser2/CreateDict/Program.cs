﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//
//namespace LanguageParser2
//{
//	public static class Program
//	{
//
//		public static MammaDictionary CreateDict() {
//		
//			DictionaryReader<NounsCreator> prepositionsReader = new DictionaryReader<NounsCreator> ("index.prepositions", null);
//			DictionaryReader<VerbsCreator> verbReader = new DictionaryReader<VerbsCreator> ("index.verb.full", "verb.exc");
//			//DictionaryReader<AdverbCreatorr> adverbReader = new DictionaryReader<AdverbCreatorr> ("index.adv", null);
//
//			//DictionaryReader<AdjectiveCreator> adjectiveReader = new DictionaryReader<AdjectiveCreator> ("index.adj", "adj.exc");
//			//DictionaryReader<NounsCreator> nounReader = new DictionaryReader<NounsCreator> ("not_used/index2.noun", "noun.exc");
//			DictionaryReader<NounsCreator> nounReader = new DictionaryReader<NounsCreator> ("test/index.noun.story", "noun.exc");
//			DictionaryReader<AdjectiveCreator> adjectiveReader = new DictionaryReader<AdjectiveCreator> ("test/index.adj.story", "adj.exc");
//
//			MammaDictionary dictionary = new MammaDictionary ();
//
//			nounReader.Read (dictionary);
//			prepositionsReader.Read (dictionary);
//			verbReader.Read (dictionary);
//			adjectiveReader.Read (dictionary);
//			//adverbReader.Read (dictionary);
//
//			return dictionary;
//
//		}
//
//
//		public static MammaDictionary CreateTestDict() {
//
//			DictionaryReader<NounsCreator> nounReader = new DictionaryReader<NounsCreator> ("test/index.noun", "noun.exc");
//			DictionaryReader<VerbsCreator> verbReader = new DictionaryReader<VerbsCreator> ("index.common.verb", "verb.exc");
//			DictionaryReader<AdjectiveCreator> adjectiveReader = new DictionaryReader<AdjectiveCreator> ("test/index.adj", "adj.exc");
//			DictionaryReader<AdverbCreatorr> adverbReader = new DictionaryReader<AdverbCreatorr> ("test/index.adv", null);
//
//			MammaDictionary dictionary = new MammaDictionary ();
//
//			nounReader.Read (dictionary);
//			verbReader.Read (dictionary);
//			adjectiveReader.Read (dictionary);
//			adverbReader.Read (dictionary);
//
//			return dictionary;
//
//		}
//
//		public static void MainX (string[] args)
//		{
//
//
//
//			//dictionary.Print ();
//			Serializer s = new Serializer();
//
//
//			MammaDictionary dictionary = //Parse ();
//				s.ReadFromBinaryFile<MammaDictionary> ("dict.data");
//
//			//s.WriteToBinaryFile ("dict.data", dictionary);
//
//			Console.WriteLine ("--------------");
//
//			//dictionary.Print ();
//				//.SerializeObject (dictionary, "dict.xml");
//
//			string input = "xxx";
//			Console.WriteLine("...done. Type a word or '' to exit.");
//
//
//
//			while (input != "") {
//
//				input = Console.ReadLine ();
//
//				if (input.Length == 0) {
//
//					break;
//
//				}
//
//				var words = dictionary.Find (input).ToList();
//
//				if (words.Count == 0) {
//
//					Console.WriteLine ($"(Not found: {input})");
//
//				} else {
//
//					PrintWords (words);
//
//					Console.WriteLine ("");
//
//				}
//
//			}
//		}
//
//		public static void PrintWords(IEnumerable<IWord> words, int level = 1) {
//
//			foreach (IWord word in words) {
//
//				for(int i = 0; i < level; i++) { Console.Write ("-"); }
//
//				if (word.Parent != null) {
//
//					Console.Write ($" ({word.Parent.Value}) ");
//
//				}
//
//				Console.WriteLine ($"{word.Value} [{word.Type}:{word.Subtype}]");
//
//				if (word.Children.Count() > 0) {
//
//					PrintWords (word.Children, level + 1);
//
//				}
//
//
//			}
//		}
//
//	}
//
//}
//
