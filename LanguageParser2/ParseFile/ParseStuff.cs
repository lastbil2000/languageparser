﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using R2Core;
using Dictionary;
using R2Core.Data;
using R2Core.Network;

namespace LanguageParser2
{

	public static class TokenProgram
	{
		static void WriteStory(string story, string outputTextFile) {
		
			File.WriteAllText (outputTextFile, story);
		
		}

		public static void Main (string[] args)
		{
			
			string dir = "../../../data/sources";

			int port = 4440;

			if (args.Length > 0) {
			
				port = int.Parse(args[0]);

				if (args.Length > 1) {
				
					dir = args [1];

				}

			}
	
//			CrossRemoveWords x = new CrossRemoveWords ("/home/olga/workspace/LanguageParser2/data/sources", "49.common.verbs.txt");
//			x.Exclude ("index.noun", "index.noun.unverbified");
//			return;
			Log.Instantiate ("log");
			Log.Instance.AddLogger (new SimpleConsoleLogger ("console", 50));

			Tokenizer t = new Tokenizer (dir);

			//if (doEverything) { t.Load (); }
			//t.Connect("127.0.0.1", 4242);
			t.Load ();

			StoryCreator s = new StoryCreator (".", dir);
			s.Load ();

			RemoteThing thing = new RemoteThing (t, s);

			LanguageServer server = new LanguageServer (thing, port);
			server.Start ();
			//Console.WriteLine(s.ConcatinateStory (s.CreateStory ("story.json")));
//////
//			string[] stories = new string[] { "red.riding.hood.txt", "beauty.beast.txt", "cinderella.txt", "pocahontas.txt", 
//				"mermaid.txt", "pinocchio.txt", "mulan.txt", "rapunzel.txt", "arthur.txt", "rumpel.txt", "rabbit.txt" ,
//				"robin.txt", "duck.txt", "emperor.txt", "spider.txt", "brothers.txt", "snow.queen.txt", "dumpling.txt", "buried.treasure.txt"};
//			string[] stories = new string[] { "buried.treasure.txt" };
//			string[] stories = new string[] { "beauty.beast.txt" };
			//string[] stories = new string[] { "red.riding.hood.txt" };
			string[] stories = new string[] { };
			foreach (string input in stories) {


				string json = t.Tokenize (File.ReadAllText(input));

				if (json != null) {
				
					WriteStory(s.CreateStory (json), $"{input}.random.txt");

				}

			}

			Console.WriteLine ("\ntype in a filename (without .txt)");
			string line = "xx";

			while ((line = Console.ReadLine ()) != "") {
			
				string fileName = $"{line}.txt";
				if (File.Exists (fileName)) { 

					string jsonFilename = t.Tokenize (fileName);
					if (jsonFilename != null) {
					
						WriteStory(s.CreateStory (jsonFilename), $"{line}.random.txt");

					}

						
				} else { Console.WriteLine ($"{fileName} does not exist."); }

			}

			server.Stop ();
		}

	}

//	public static class ParseStuffXX
//	{
//		public static void MainZZZ (string[] args) {
//		
//
//			FileReader reader = new FileReader ("word.replacement");
//
//			TokenCreator creator = new TokenCreator (reader.GetWordsDictionary());
//			creator.PrepareForCreateXml ("full.story.txt", "full.story.replaced.txt");
//			creator.CreateXml ("full.story.replaced.txt", "full.story.xml");
//
//			//Dictionary d = Program.CreateTestDict ();
//			MammaDictionary d = Program.CreateDict ();
//
//			WordList ignoreList = new WordList ();
//			ignoreList.Load ("noun.ignore");
//			ignoreList.Load ("verb.ignore");
//			ignoreList.Load ("adverb.ignore");
//			ignoreList.Load ("prepositions.ignore");
//
//			string storyFile = "full.story.xml";
//			//string storyFile = "test/test.xml"; // "full.story.xml"
//			TokenReader r = new TokenReader (storyFile, d, ignoreList);
//
//			List<IWord> wordz = new List<IWord> ();
//
//			Console.WriteLine ("parsing xml");
//
//			r.Preprocess ();
//
//			foreach (IWord word in r.Read ()) {
//
//				wordz.Add (word);
//				//Console.Write ($"{word.Value} ");
//
//
//			}
//
//			Console.WriteLine ("---");
//
//			IList<PreparedWord> preparedList = new List<PreparedWord> ();
//
//			int id = 0;
//			foreach (IWord word in wordz) {
//			
//
//				PreparedWord sameWord = preparedList.FirstOrDefault (w => 
//					w.Lemma == (word.Parent?.Value ?? word.Value) && w.Type == word.Type
//				);
//
//				PreparedWord prepared =  new PreparedWord (id, word);
//
//				if (sameWord.Lemma != null) {
//					
//					// there was another word of this type
//
//					prepared = new PreparedWord (sameWord.Id, word);
//				
//				} else {
//				
//					id++;
//
//				}
//
//				preparedList.Add (prepared);
//
//			}
//
//			string outputFilename = "story.json";
//
//			File.WriteAllText(outputFilename, JsonConvert.SerializeObject(preparedList));
//		}
//
//	}

}

