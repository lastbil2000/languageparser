﻿using System;
using System.Collections.Generic;
using Dictionary;

namespace LanguageParser2
{
	public class ParsedWord: IWord
	{
		
		protected string m_value;
		protected GramType m_type;
		protected GramType m_subtype;

		public string Value { get { return m_value; } }

		public GramType Subtype { get { return m_subtype; } }

		public GramType Type { get { return m_type; } }

		public IWord Parent { get { return null; } }

		public IEnumerable<IWord> Children { get { yield break; } }

		public void AddChild (IWord child) {

			throw new NotImplementedException ();

		}

		public ParsedWord (string value, GramType type, GramType? subtype = null) {

			m_value = value;
			m_type = type;
			m_subtype = subtype ?? type;

		}

	}
}

