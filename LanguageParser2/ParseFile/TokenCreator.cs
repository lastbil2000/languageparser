﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using R2Core;

namespace LanguageParser2
{
	public class TokenCreator
	{
		IDictionary<string, string> m_replacements;

		public TokenCreator (IDictionary<string, string> replacements)
		{

			m_replacements = replacements;

		}

		/// <summary>
		/// Fix the text according to the replacements
		/// </summary>
		/// <returns>The text.</returns>
		/// <param name="inputText">Input text.</param>
		public string PrepareText(string text) {
			
			foreach (KeyValuePair<string, string> replace in m_replacements) {

				Regex reg = new Regex (replace.Key);

				text = reg.Replace (text, replace.Value);

			}

			return text;

		}

		public string CreateXmlString(string preparedText) {
			
			var c = new OpenErClient ();

			string identified = c.Send (preparedText, OpenErType.Identify);
			string tokenized = c.Send (identified, OpenErType.Tokenize);
			return c.Send (tokenized, OpenErType.PosTag);

		}

		//Obsolete
		public void _PrepareForCreateXml(string inputFileName, string outputFileName) {
		
			string line;

			using (StreamReader reader = new StreamReader (inputFileName)) {

				using (StreamWriter writer = new StreamWriter (outputFileName)) {

					while ((line = reader.ReadLine ()) != null) {
				
						foreach (KeyValuePair<string, string> replace in m_replacements) {
						
							Regex reg = new Regex (replace.Key);
								
							line = reg.Replace (line, replace.Value);

						}

						writer.WriteLine (line);

					}
				
				}

			}

		}

		//Obsolete
		public void _CreateXml(string inputFileName, string outputFileName) {

			Console.WriteLine($"Creating XML file by executing 'create_xml.sh' using '{inputFileName}'");
			ProcessStartInfo psi = new ProcessStartInfo();
			psi.FileName = "create_xml.sh";
			psi.UseShellExecute = false;
			psi.RedirectStandardOutput = true;

			psi.Arguments = $"{inputFileName} {outputFileName}";
			Process p = Process.Start(psi);
			string strOutput = p.StandardOutput.ReadToEnd();
			p.WaitForExit();

			Console.WriteLine(strOutput);

		}

	}
}

