﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Dictionary;
using R2Core;

namespace LanguageParser2
{
	public class Tokenizer
	{
		private IWordContainer m_dictionary;
		private string m_outputDir;
		private string m_dataDir;
		private string m_tempDir;
		private WordList m_ignoreList;
		private TokenCreator m_creator;

		public Tokenizer (string dataDir, string outputDir = null, string tempDir = null)
		{

			tempDir = tempDir ?? ".";
			m_outputDir = outputDir ?? ".";
			m_dataDir = $"{dataDir}{Path.DirectorySeparatorChar}Data{Path.DirectorySeparatorChar}";
			m_tempDir = $"{tempDir}{Path.DirectorySeparatorChar}Temp{Path.DirectorySeparatorChar}";

		}

		/// <summary>
		/// Connect to a dictionary server.
		/// </summary>
		/// <param name="host">Host.</param>
		/// <param name="port">Port.</param>
		public void Connect(string host, int port) {
		
			Console.WriteLine ("Connecting to dictionary server...");

			RemoteDictionary dictionary = new RemoteDictionary (host, port);
			dictionary.Start ();
			while(!dictionary.Ready) { System.Threading.Thread.Sleep (200); Console.Write ("."); } 
			m_dictionary = dictionary;

			m_ignoreList = LoadIgnoreList ();
			m_creator = new TokenCreator (new FileReader ($"{m_dataDir}word.replacement").GetWordsDictionary());

		}

		/// <summary>
		/// Load local dictionary (very slow start, but requires not an online DictionaryServer)
		/// </summary>
		public void Load() {

			Console.WriteLine ("Loading dictionary...");
			m_dictionary = CreateDictionary();

			m_ignoreList = LoadIgnoreList ();
			m_creator = new TokenCreator (new FileReader ($"{m_dataDir}word.replacement").GetWordsDictionary());

		}


		private WordList LoadIgnoreList() {
		
			var ignoreList = new WordList ();
			ignoreList.Load ($"{m_dataDir}noun.ignore");
			ignoreList.Load ($"{m_dataDir}verb.ignore");
			ignoreList.Load ($"{m_dataDir}adverb.ignore");
			ignoreList.Load ($"{m_dataDir}adjective.ignore");
			ignoreList.Load ($"{m_dataDir}prepositions.ignore");

			return ignoreList;

		}

		private IWordContainer CreateDictionary() {
		
			DictionaryReader<NounsCreator> prepositionsReader = new DictionaryReader<NounsCreator> ($"{m_dataDir}index.prepositions", null);
			DictionaryReader<VerbsCreator> verbReader = new DictionaryReader<VerbsCreator> ($"{m_dataDir}index.verb.full", $"{m_dataDir}verb.exc");
			DictionaryReader<LocationsCreator> locationReader = new DictionaryReader<LocationsCreator> ($"{m_dataDir}index.location", null);

			DictionaryReader<AdjectiveCreator> adjectiveReader = new DictionaryReader<AdjectiveCreator> ($"{m_dataDir}index.adj", $"{m_dataDir}adj.exc");

			// The nouns creator has to be created manualy in order to contain the uncountable nouns list
			WordList unclountableNouns = new WordList ();
			unclountableNouns.Load ($"{m_dataDir}noun.uncountable");
			NounsCreator nounsCreator = new NounsCreator (unclountableNouns.Words);

			DictionaryReader<NounsCreator> nounReader = new DictionaryReader<NounsCreator> ($"{m_dataDir}index.noun", $"{m_dataDir}noun.exc", nounsCreator);

			IWordContainer dictionary = new PappaDictionary ("dictionary");

			nounReader.Read (dictionary);
			prepositionsReader.Read (dictionary);
			verbReader.Read (dictionary);
			adjectiveReader.Read (dictionary);
			locationReader.Read (dictionary);

			return dictionary;

		}

//		private string CreateXmlFile(string inputFileName) {
//		
//			FileReader reader = new FileReader ($"{m_dataDir}word.replacement");
//
//			TokenCreator creator = new TokenCreator (reader.GetWordsDictionary());
//			string replacedFileName = $"{m_tempDir}{inputFileName}.replaced";
//			string xmlFileName = $"{m_tempDir}{inputFileName}.xml";
//			creator.PrepareForCreateXml ($"{m_outputDir}{Path.DirectorySeparatorChar}{inputFileName}", replacedFileName);
//			creator.CreateXml (replacedFileName, xmlFileName);
//
//			return xmlFileName;
//
//		}

		private IEnumerable<IWord> ParseWords(string xmlString) {
		
			TokenReader r = new TokenReader (xmlString, m_dictionary, m_ignoreList);

			if (!r.Preprocess ()) {
			
				Log.e ("Argh! Unable to preprocess");
				return new List<IWord> ();

			}

			return r.Read ();

		}

		private IList<PreparedWord> CreatePreparedList(IEnumerable<IWord> words) {
		
			IList<PreparedWord> preparedList = new List<PreparedWord> ();

			// Keeps track of name genetives
			IList<string> nameList = new List<string> ();

			int id = 0;
			foreach (IWord word in words) {
				
				PreparedWord sameWord = preparedList.FirstOrDefault (w => w.IsSame(word) );

				PreparedWord prepared = new PreparedWord (id, word);

				if (sameWord.Lemma != null) {
					// there was another word of this type

					prepared = new PreparedWord (sameWord.Id, word);

				}else { id++; }

				preparedList.Add (prepared);

			}

			return preparedList;

		}


		public string Tokenize(string text) {

			// Prepare text for xml creation (doing some replacements).
			string preparedText = m_creator.PrepareText (text);

			// Create XML using OpenEr servers 
			string xml = m_creator.CreateXmlString (preparedText);

			IEnumerable<IWord> wordz = ParseWords (xml);

			IList<PreparedWord> preparedList;

			try {
			
				preparedList = CreatePreparedList (wordz);

			} catch (System.Net.Sockets.SocketException ex) {

				Console.WriteLine ("Connection issues. Dictionary client timed out.");
				return null;

			}

			return JsonConvert.SerializeObject(preparedList);

		}

	}

	static class WordExtensions {
	
		/// <summary>
		/// Returns true if the other word is the same word (i.e. eat, eats; Morgoth, Morgoths)
		/// </summary>
		/// <returns><c>true</c> if is same the specified self other; otherwise, <c>false</c>.</returns>
		/// <param name="self">Self.</param>
		/// <param name="other">Other.</param>
		public static bool IsSame(this PreparedWord self, IWord other) {
		
			if (self.Type != other.Type) { return false; }

			//TODO: fix this for names in genetive (Beast Beasts)

			return (self.Lemma == (other.Parent?.Value ?? other.Value));

		}

	}

}

