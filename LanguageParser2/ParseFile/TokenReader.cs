﻿using System;
using System.Xml.Linq;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Dictionary;

namespace LanguageParser2
{
	struct GTypes {
	
		public GramType Type;
		public GramType Sub;

		private GTypes(GramType type, GramType? sub = null) {
		
			Type = type;
			Sub = sub ?? type;

		}

		public static GTypes C(GramType type, GramType? sub = null) {

			return new GTypes (type, sub);
				
		}
	}

	public class TokenReader
	{

		private Regex m_nameRegexp = new Regex (@"^{A-Z}{1}[a-z\-]+$");

		XmlDocument m_doc;
		private WordList m_ignore;
		private IWordContainer m_dictionary;
		private string m_storyXml;
		private IList<XmlNode> m_nodeList;

		public TokenReader (string storyXml, IWordContainer dictionary, WordList ignore)
		{


			m_storyXml = storyXml;
			m_doc = new XmlDocument();

			m_ignore = ignore;
			m_dictionary = dictionary;

		}

		private GTypes GetType(XmlNode node) {
		
				XmlAttributeCollection a = node.Attributes;

			string morph = a ["morphofeat"]?.Value;
			string lemma = a ["lemma"]?.Value;
			string pos = a ["pos"]?.Value;

			if (lemma != null && lemma.Length > 0 && lemma.Substring(0,1).IsAlphabetic()) {

				if (morph == "NN") { return GTypes.C(GramType.Noun); }
				if (morph == "NNS") { return GTypes.C(GramType.Noun, GramType.Plural); }
				if (morph == "NN-gen") { return GTypes.C(GramType.Noun, GramType.Genetive); }

				if (morph == "JJ") { return GTypes.C(GramType.Adjective); }
				if (morph == "JJR") { return GTypes.C(GramType.Adjective); }

				if (morph == "VB") { return GTypes.C(GramType.Verb); }
				if (morph == "VBP") { return GTypes.C(GramType.Verb, GramType.Preterite); }
				if (morph == "VBN") { return GTypes.C(GramType.Verb, GramType.PastParticiple); }
				if (morph == "VBD") { return GTypes.C(GramType.Verb, GramType.Preterite); }
				if (morph == "VBZ") { return GTypes.C(GramType.Verb, GramType.Present); }
				if (morph == "VBG") { return GTypes.C(GramType.Verb, GramType.PresentContinous); }

				if (morph == "RB") { return GTypes.C(GramType.Adverb); }
				if (morph == "RBR") { return GTypes.C(GramType.Adverb, GramType.Comparative); }

				if (morph == "IN" || morph == "TO") { return GTypes.C(GramType.Preposition); }
				if (morph == "NNP" || morph == "NNPS") { return GTypes.C(GramType.Name); }
				if (morph == "NNP-gen") { return GTypes.C(GramType.Name, GramType.Genetive); }

				if (morph == "PRP") { return GTypes.C(GramType.Pronomen); }
				if (morph == "PRP$") { return GTypes.C(GramType.Pronomen, GramType.Dative); }
				if (morph == "WP") { return GTypes.C(GramType.Pronomen, GramType.Relative); }

				if (morph == "CC" ) { return GTypes.C(GramType.Conjugation); }

			}

			if (pos == "O") { 

				if (morph == "." || morph == "," || morph == "\"" || morph == "'" || morph == ";" || morph == ":") { return GTypes.C(GramType.Other, GramType.Annotation); }
				if (morph == "SYM" ) { return GTypes.C(GramType.Other, GramType.Symbol); }

				return GTypes.C(GramType.Other);

			}


			if (morph == "DT") { 
				if (lemma == "the") { return GTypes.C (GramType.Article, GramType.DT_the); } 
				if (lemma == "a") { return GTypes.C (GramType.Article, GramType.DT_a); }
				if (lemma == "an") { return GTypes.C (GramType.Article, GramType.DT_an); }

				return GTypes.C (GramType.Article, GramType.Determiner);
			
			}

			return GTypes.C (GramType.Unknown);

		}

		private bool CanTypeBeInDictionary(string name, GramType type) {

			return (type == GramType.Adjective ||
				type == GramType.Name ||
			type == GramType.Noun ||
			type == GramType.Verb ||
				type == GramType.Preposition ||
				type == GramType.Adverb) && !m_ignore.Words.Contains(name.ToLower());
			
		}

		private bool IsCrazyException(GTypes types, IWord foundWord) {

			return // words can sometimes be interpreted by that stupid engine as adjectives: 
				((foundWord != null && foundWord?.Type != types.Type) && (

					(types.Type == GramType.Adjective && foundWord.Type == GramType.Location) 
//					|| (types.Type == GramType.Adjective && foundWord.Type == GramType.Name) 
//					|| (types.Type == GramType.Adjective && foundWord.Type == GramType.Noun)  
//					|| (types.Type == GramType.Adjective && foundWord.Type == GramType.Verb) 
//					|| (types.Type == GramType.Adjective && foundWord.Type == GramType.Preposition)
				)

				)
				// Should account for uncountable nouns, which only comes in plural.
				|| (foundWord?.Subtype == GramType.Plural && types.Sub == GramType.Noun)
				;
		}

		public IEnumerable<IWord> Read() {
			
			IList<string> tmp = new List<string> ();

			foreach (XmlNode node in m_nodeList) {

				var types = GetType (node);

				string name = node.Attributes ["lemma"]?.Value;
				string originalName = node.Attributes ["originalName"]?.Value;

				// in Dictionary?
				if (CanTypeBeInDictionary (name, types.Type)) {
					
					IList<IWord> foundWords = m_dictionary.Find (name).ToList();

					if (foundWords.Count == 0) {
					
						// If no words found, make a lower case search, just to make sure no words accidently were
						// interpreted as names.
						foundWords = m_dictionary.Find (name.ToLower ()).ToList ();
					
					}

					// Check if a (base) word of the same base GramType exists.
					IWord foundWord = foundWords.FirstOrDefault (w => w.Type == types.Type) ?? foundWords.GetPrioritizedWord();

					if (//(foundWord != null && foundWord?.Type != types.Type) ||
						//foundWords.Count == 1 || 

						// Sometimes words that starts with an uppercase letter can be missinterpreted as a name
						foundWord != null && types.Type == GramType.Name ||

						// Do always let locations through (they will never be identified otherwise) 
						foundWord?.Type == GramType.Location ||

						IsCrazyException (types, foundWord) ||

						// The found word had the same subtype, indicating that the identification was correct
						foundWord?.Subtype == types.Sub) { 

						yield return foundWord; 
					
					} else {
	
						// If not, take the first available... This is due to the fact that the parser confuses nouns for verbs.
						yield return foundWord?.Children.FirstOrDefault (w => w.Subtype == types.Sub) ?? new ParsedWord (originalName ?? name, types.Type, types.Sub); 

					}

				} else {

					// if not in Dictionary, add it as a ParsedWord using the retrieved attributes.
					yield return new ParsedWord (originalName ?? name, 

						// If the ignore list contains the word. Make it un-transcribable
						m_ignore.Words.Contains(originalName?.ToLower() ?? name.ToLower()) ? GramType.KeepOriginal : types.Type, 

						types.Sub);

				}

			}

		}


		public bool Preprocess() {
		
			m_nodeList = new List<XmlNode> ();

			try {
				
				m_doc.LoadXml(m_storyXml);

			} catch (XmlException exception) {
			
				Console.WriteLine ($"Xml errol: {exception.Message}");
				return false;

			}

			string originalName = null;

			foreach (XmlNode pathsNode in m_doc.SelectNodes ("/KAF/terms")) {

				GTypes previousType = new GTypes () { Type = GramType.Unknown, Sub = GramType.Unknown };

				int i = 0;
				foreach (XmlNode node in pathsNode.ChildNodes) { 
					
					if (node.NodeType == XmlNodeType.Comment) {

						// The original word is stored in the comment above each terms tag.
						originalName = node.Value.Trim();

					}

					if (node.Name == "term" && node.Attributes != null) {

						var types = GetType (node);
						string name = node.Attributes ["lemma"]?.Value;

						if ((types.Type == GramType.Name && previousType.Type == GramType.Name) ||
						    (previousType.Type == GramType.Noun && previousType.Sub == GramType.Noun && types.Type == GramType.Noun)) {

							// Do not add multiple names (i.e. only use John in "[John] [Doe]"	
							// Do not add multiple nouns (i.e. riding cloak)

							m_nodeList[i - 1].Attributes ["lemma"].Value = m_nodeList[i - 1].Attributes ["lemma"].Value + " " + name;
							m_nodeList[i - 1].Attributes ["originalName"].Value = m_nodeList[i - 1].Attributes ["originalName"].Value + " " + originalName;
							continue;

						}

						previousType = types;

						if (name != null) {

							if (types.Type == GramType.Unknown && types.Sub == GramType.Unknown && 
								m_nameRegexp.IsMatch(originalName)) {
							
								node.Attributes ["morphofeat"].Value = "NNP";

							}

							if (originalName == "'s" &&
							    node.Attributes ["morphofeat"]?.Value == "VBZ") {
							
								originalName = "is";
								node.Attributes ["lemma"].Value = "are";

							} else if (i > 0 && originalName == "'s" &&
							           node.Attributes ["morphofeat"]?.Value == "POS") {

								// Genetive
								m_nodeList[i - 1].Attributes ["originalName"].Value = m_nodeList[i - 1].Attributes ["originalName"].Value + "s";
								m_nodeList[i - 1].Attributes ["morphofeat"].Value += "-gen";

								continue;

							}

							node.Attributes.Append (m_doc.CreateAttribute ("originalName"));
							node.Attributes ["originalName"].Value = originalName;

							i++;
							m_nodeList.Add (node);

						}
					}
				}
			}

			return true;

		}

	}

	public static class IEnumerable {
		
		public static IWord GetPrioritizedWord<T>(this IEnumerable<T> self) where T: IWord {
		
			foreach(GramType type in new GramType[] {GramType.Location, GramType.Noun, GramType.Adjective, GramType.Verb}) {
			
				foreach (IWord word in self) {
				
					if (word.Type == type) {
					
						return word;

					}

				}

			}

			return default(T);

		}

	}

}

//						if (types.Type == GramTypes.Adjective) {
//						
//							if (!tmp.Contains(node.Attributes ["lemma"]?.Value)) {
//								
//								tmp.Add(node.Attributes ["lemma"]?.Value);
//								Console.WriteLine (node.Attributes ["lemma"]?.Value); 
//
//							}
//								
//						}