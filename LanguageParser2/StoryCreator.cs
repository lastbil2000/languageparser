﻿using System;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using Dictionary;

namespace LanguageParser2
{

	public static class IWordExtensions {
	
		public static IWord GetWord(this IWord self, GramType subtype) {

			return self.Children?.FirstOrDefault (w => w.Subtype == subtype) ?? self;

		}

	}

	public class StoryCreator
	{

		private RandomWords m_dictionary;
		private string m_baseDir;
		private string m_storyDir;
		private string m_dataDir;
		private string m_tempDir;

		public StoryCreator (string baseDir = null, string dataDir = null)
		{

			m_baseDir = baseDir ?? ".";
			dataDir = dataDir ?? ".";
			m_dataDir = $"{dataDir}{Path.DirectorySeparatorChar}Data{Path.DirectorySeparatorChar}";
			m_storyDir = $"{m_baseDir}{Path.DirectorySeparatorChar}News{Path.DirectorySeparatorChar}";
			m_tempDir = $"{m_baseDir}{Path.DirectorySeparatorChar}Temp{Path.DirectorySeparatorChar}";

		}

		public void Load() {

			Console.WriteLine ("Loading dictionary...");
			m_dictionary = CreateDictionary();

		}

		public RandomWords CreateDictionary() {

			//DictionaryReader<NounsCreator> prepositionsReader = new DictionaryReader<NounsCreator> ($"{m_storyDir}index.prepositions", null);
			DictionaryReader<VerbsCreator> verbReader = new DictionaryReader<VerbsCreator> ($"{m_storyDir}index.verb", $"{m_dataDir}verb.exc");
			DictionaryReader<AdjectiveCreator> adjectiveReader = new DictionaryReader<AdjectiveCreator> ($"{m_storyDir}index.adj", $"{m_dataDir}adj.exc");
			DictionaryReader<LocationsCreator> locationReader = new DictionaryReader<LocationsCreator> ($"{m_storyDir}index.location", null);
			DictionaryReader<NameCreator> nameReader = new DictionaryReader<NameCreator> ($"{m_storyDir}index.name", null);

			WordList unclountableNouns = new WordList ();
			unclountableNouns.Load ($"{m_dataDir}noun.uncountable");
			NounsCreator nounsCreator = new NounsCreator (unclountableNouns.Words);

			DictionaryReader<NounsCreator> nounReader = new DictionaryReader<NounsCreator> ($"{m_storyDir}index.noun", $"{m_dataDir}noun.exc", nounsCreator);


			RandomWords dictionary = new RandomWords ("random_dictionary");

			//prepositionsReader.Read (dictionary);

			nounReader.Read (dictionary);
			verbReader.Read (dictionary);
			adjectiveReader.Read (dictionary);
			nameReader.Read (dictionary);
			locationReader.Read (dictionary);

			return dictionary;

		}

		public string CreateStory(string inputJson) {
		
			return ConcatinateStory (CreateStoryWords (inputJson));

		}

		/// <summary>
		/// Creates the final list of word with the replacements
		/// </summary>
		/// <returns>The story words.</returns>
		/// <param name="inputJsonFile">Input json file.</param>
		public IEnumerable<string> CreateStoryWords(string inputJson) {

			IList<PreparedWord> story = JsonConvert.DeserializeObject<IList<PreparedWord>> (inputJson);
			IDictionary<int, IWord> addedWords = new Dictionary<int, IWord> ();
			IList<string> finalStory = new List<string> ();

			for (int i = 0; i < story.Count; i++) {

				PreparedWord word = story [i];

				IWord random = m_dictionary.GetRandom (word.Type, word.Subtype);

				if (random != null && i > 0 && (random.Type == GramType.Noun || random.Type == GramType.Adjective)) {
				
					// Fix an/a determiner.

					if ((story [i - 1].Subtype == GramType.DT_a || story [i - 1].Subtype == GramType.DT_an)) {
					
						finalStory[i - 1] = random.Value.StartsWithAWovel () ? "an" : "a";

					}
				 
				}

				if ((!word.Use || random == null)) {

					// Use original
					finalStory.Add(word.Lemma);

				} else {
					
					if (addedWords.ContainsKey (word.Id)) {

						random = addedWords [word.Id].GetWord (word.Subtype);

					} else  {

						addedWords [word.Id] = random.Parent ?? random;

					}

					if (random.Type == GramType.Name) {
					
						finalStory.Add(random.Value.UppercaseEachFirstLetter());

					} else {
					
						finalStory.Add (random.Value);

					}

				}

			}

			return finalStory;

		}

		public string ConcatinateStory(IEnumerable<string> words) {
		
			string story = "";

			Regex isAlpha = new Regex (@"^[a-zA-Z0-9]+");

			foreach (string word in words) {
			
				if (isAlpha.IsMatch (word)) {
				
					story += " ";

				}

				story += word.Trim ();

				if (word.Trim () == ".") {
				
					story += "\n";

				}

			}

			return story;
		
		}

	}

}

