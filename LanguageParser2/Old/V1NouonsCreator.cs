﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LanguageParser2
{
	public class V1NounsCreater: V1BaseWordsCreator {


		public V1NounsCreater(string word, IEnumerable<string> exceptions): base(word, exceptions) {
		}
		//A man with no name tries to reunite with his family in Purgatory.

		public override IEnumerable<IWord> Generate() {
		
			IList<IWord> words = new List<IWord> ();

			IWord baseWord = new Word (Word, "noun");

			words.Add (baseWord);
			string exception = Exceptions.FirstOrDefault ();

			if (exception != null) {

				words.Add (new Word (exception, "plural", baseWord));

			} else {
			
				words.Add (new Word (GetPlural (), "plural", baseWord));

			}

			return words;

		}

		private string GetPlural() {
		
			if (Word.EndsWith ("ey")) {

				return Word.Substring (0, Word.Length - 2) + "ies";

			} else if (Word.EndsWith ("y")) {

				return Word.Substring (0, Word.Length - 1) + "ies";

			} else if (Word.EndsWith ("s")) {

				return Word + "es";

			}

			return Word + "s";

		}



	}
}

