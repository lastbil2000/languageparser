﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace LanguageParser2
{
	public class V1DictionaryReader
	{
		private StreamReader m_reader;
		private string m_fileName;
	
		private Dictionary m_dictionary;

		private  IDictionary<string, string> m_exceptionFiles;

		public V1DictionaryReader  (string fileName, IDictionary<string, string> exceptionFiles)
		{
			if (!File.Exists (fileName)) {

				throw new IOException ("Dictionary not found: " + fileName);

			}

			m_fileName = fileName;
			m_dictionary = new Dictionary ();
			m_exceptionFiles = exceptionFiles;
		}

		public Dictionary Read() {

			IDictionary<string,  IDictionary<string, IList<string>>> exceptions = new Dictionary<string, IDictionary<string, IList<string>>> ();

			foreach (KeyValuePair<string, string> exception in m_exceptionFiles) {
			
				exceptions [exception.Key] = exception.Value.AsExceptionsFile ();

			}

			V1WordFactory factory = new V1WordFactory (exceptions);

			string line = null;

			using (m_reader = new StreamReader (m_fileName)) {

				while ((line = m_reader.ReadLine ()) != null) {

					V1IWordsCreator creator = factory.GetCreator (line);

					foreach (IWord word in creator?.Generate () ?? new List<IWord>()) {
					
						m_dictionary.AddWord (word);

					}

				}

			}

			return m_dictionary;

		}
	}
}

