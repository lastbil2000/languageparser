﻿using System;

namespace LanguageParser2
{
	public interface V1IWordsCreator
	{

		System.Collections.Generic.IEnumerable<IWord> Generate();

	}
}

