﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LanguageParser2
{

	public enum VerbTenses {
	
		Past,
		Present,
		Participle
	}

	public class V1VerbsCreator: V1BaseWordsCreator
	{

		public V1VerbsCreator(string word, IEnumerable<string> exceptions): base(word, exceptions) {
		}

		public override IEnumerable<IWord> Generate() {

			IList<IWord> words = new List<IWord> ();

			IWord baseWord = new Word (Word, "verb");

			words.Add (baseWord);
			words.Add (new Word (GetPresent (), "present", baseWord));
			words.Add (new Word (GetPresentParticiple (), "present_participle", baseWord));

			IList<string> exceptions = new List<string> (Exceptions);

			if (exceptions.Count > 0) {
		
				words.Add (new Word (exceptions [0], "past", baseWord));
				words.Add (new Word (exceptions [1], "past_participle", baseWord));

			} else {
			
				words.Add (new Word (GetPast (), "past", baseWord));
				words.Add (new Word (GetPast (), "past_participle", baseWord));

			}

			return words;

		}

		private string GetPresent() {
		
			if (Word.EndsWith ("y")) {

				return Word.Substring (0, Word.Length - 1) + "ies";

			} else if (Word.EndsWith ("s")) {

				return Word + "es";

			}
		
			return Word + "s";

		}

		//killing
		private string GetPresentParticiple() {

			if (Word.EndsWith ("e")) {

				return Word.Substring (0, Word.Length - 1) + "ng";

			}

			return Word + "ing";
		
		}

		//killed
		private string GetPast() {

			if (Word.EndsWith ("e")) {

				return Word + "d";

			}

			return Word + "ed";

		}

	}
}

