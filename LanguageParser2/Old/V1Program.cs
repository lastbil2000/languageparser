﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace LanguageParser2
{
	class V1MainClass
	{

		public static void FixFile(string inputFile, string outputFile) {
		
			string line = null;
			StreamReader reader;

			if (File.Exists(outputFile)) { File.Delete (outputFile); }

			//File.Create (outputFile);

			using (StreamWriter writer = new StreamWriter (outputFile)) {
				
				using (reader = new StreamReader (inputFile)) {

					while ((line = reader.ReadLine ()) != null) {

						string[] parts = line.Split (null);
						Array.Reverse (parts);
						writer.WriteLine(string.Join(" ", parts));
					}

			
				}
			}

		}
		public static void V1Main (string[] args)
		{

			//FixFile ("noun.exc", "noun2.exc"); return;



			V1DictionaryReader reader = new V1DictionaryReader ("core-wordnet.txt", 
				new Dictionary<string, string> () {{"noun", "noun2.exc"}, {"verb", "verb2.exc"}});
		
			Console.WriteLine ("Parsing...");

			Dictionary dictionary = reader.Read ();

			string input = "xxx";
			Console.WriteLine("...done. Type a word or '' to exit.");


			while (input != "") {

				input = Console.ReadLine ();
				if (input.Length == 0) {
				
					break;

				}

				var words = dictionary.Find (input);

				if (words.Count == 0) {
				
					Console.WriteLine ($"(Not found: {input})");
				
				} else {
				
					PrintWords (words);

					Console.WriteLine ("");

				}

				//Console.WriteLine ($" - {word}");
			}

		}

		public static void PrintWords(IEnumerable<IWord> words, int level = 1) {
			
			foreach (IWord word in words) {

				for(int i = 0; i < level; i++) { Console.Write ("-"); }

				if (word.Parent != null) {
				
					Console.Write ($" ({word.Parent.Value}) ");
				
				}

				Console.WriteLine ($"{word.Value} [{word.Type}:{word.Subtype}]");

				if (word.Children.Count() > 0) {

					PrintWords (word.Children, level + 1);

				}


			}
		}
	}
}
