﻿using System;
using System.Collections.Generic;

namespace LanguageParser2
{
	public abstract class V1BaseWordsCreator: V1IWordsCreator
	{
		
		private string m_word;
		private IEnumerable<string> m_exceptions;

		protected string Word { get { return m_word; } }
		protected IEnumerable<string> Exceptions { get { return m_exceptions; } }

		public V1BaseWordsCreator (string word, IEnumerable<string> exceptions) {

			m_word = word;
			m_exceptions = exceptions ?? new List<string>();

		}

		public abstract IEnumerable<IWord> Generate ();

	}
}

