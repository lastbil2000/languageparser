﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LanguageParser2
{
	public class V1WordFactory
	{
		private Regex m_wordRegexp = new Regex (@"\[[A-Za-z\-\'\ ]+\]") ;

		int counter = 0;

		private IDictionary<string, IDictionary<string, IList<string>>> m_exceptions;

		public V1WordFactory (IDictionary<string, IDictionary<string, IList<string>>> exceptions) {

			m_exceptions = exceptions;

		}

		public V1IWordsCreator GetCreator(string line) {

			string word = GetWord (line.ToLower())?.Replace("[","").Replace("]","");

			if (word == null) {
			
				Console.WriteLine ($"Unable to match word for line: '{line}'");
				return null;

			}

			if (line.Trim ().ToLower ().StartsWith ("a")) {
			
				//TODO
				return null;
			}

			if (line.Trim ().ToLower().StartsWith ("n")) {

				return new V1NounsCreater (word, GetExceptions("noun", word));

			} else if (line.Trim ().ToLower().StartsWith ("v")) {
				
				return new V1VerbsCreator (word, GetExceptions("verb", word));

			}

			if (counter++ % 100 == 0) { Console.Write ($"."); }

			Console.WriteLine ($"Unable to find word type for line: '{line}'");
			return null;

		}

		private IEnumerable<string> GetExceptions(string type, string word) {
		
			return m_exceptions [type].ContainsKey (word) ? m_exceptions [type] [word] : null;

		}


		private string GetWord(string line) {

			Match wordMatch = m_wordRegexp.Match (line);

			if (wordMatch.Success) {
			
				return wordMatch.Value;

			}

			return wordMatch.Success ? wordMatch.Value : null;

		}
	}
}

