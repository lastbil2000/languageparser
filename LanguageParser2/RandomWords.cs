﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dictionary;
using R2Core.Device;

namespace LanguageParser2
{

	class RandomWord: IWord {
	
		/// <summary>
		/// Use this in order to not use the same word at multiple replacements.
		/// </summary>
		public bool Used = false;

		private IWord m_word;

		public string Value { get { return m_word.Value; } }

		public GramType Type { get { return m_word.Type; } }
		public GramType Subtype { get { return m_word.Subtype; } }

		public IWord Parent { get { return m_word.Parent; } }

		public IEnumerable<IWord> Children { get { return m_word.Children; } }

		public void AddChild (IWord child) { 
		
			m_word.AddChild (child);

		}

		public RandomWord(IWord word) {
		
			m_word = word;
			this.Used = false;
		}

	}

	public class RandomWords: DeviceBase, IWordContainer {
		
		private IDictionary<GramType, IList<RandomWord>> m_words;
		private Random m_random;

		public RandomWords (string id): base(id) {

			m_words = new Dictionary<GramType, IList<RandomWord>> ();
			m_random = new Random ();

		}

		public void AddWord (IWord word) {

			if (word.Parent == null) {
			
				if (!m_words.ContainsKey (word.Type)) { m_words [word.Type] = new List<RandomWord> (); }

				m_words [word.Type].Add (new RandomWord(word));

			}

		}

		public IEnumerable<IWord> Find (string word) {

			foreach(IList<IWord> list in m_words.Values) {

				foreach(IWord w in list) {
				
					if (word != null && w.Value?.Trim() == word?.Trim()) {

						yield return w;
					
					}
				
				}

			}

		}

		public IWord GetRandom(GramType type, GramType subtype) {
		
			if (!m_words.ContainsKey(type)) { return null; }

			//Console.Write ($" {m_words [type].Where (w => !w.Used).Count ()} ");
			if (m_words [type].Where (w => w.Used).Select(w => w).Count () == m_words [type].Count) {

				// All words have been used. reset
				for (int i = 0; i < m_words [type].Count; i++) {
				
					m_words [type] [i].Used = false;

				}

			}

			RandomWord randomWord = null;

			while (randomWord == null) {
			
				int random = m_random.Next (m_words [type].Count);

				//Console.Write ($" [{random}] " );
				if (!m_words [type] [random].Used) {
					
					m_words [type] [random].Used = true;
					randomWord = m_words [type] [random];

				}

			}
			//= m_words [type] [m_random.Next (m_words [type].Count)];

			if (type == subtype) { return randomWord; }

			return randomWord.Children.FirstOrDefault( w => w.Subtype == subtype );

		}
	
	}
}

